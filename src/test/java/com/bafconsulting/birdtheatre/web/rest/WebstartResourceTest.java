package com.bafconsulting.birdtheatre.web.rest;

import com.bafconsulting.birdtheatre.Application;
import com.bafconsulting.birdtheatre.domain.Webstart;
import com.bafconsulting.birdtheatre.repository.WebstartRepository;
import com.bafconsulting.birdtheatre.repository.search.WebstartSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the WebstartResource REST controller.
 *
 * @see WebstartResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class WebstartResourceTest {


    @Inject
    private WebstartRepository webstartRepository;

    @Inject
    private WebstartSearchRepository webstartSearchRepository;

    private MockMvc restWebstartMockMvc;

    private Webstart webstart;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        WebstartResource webstartResource = new WebstartResource();
        ReflectionTestUtils.setField(webstartResource, "webstartRepository", webstartRepository);
        ReflectionTestUtils.setField(webstartResource, "webstartSearchRepository", webstartSearchRepository);
        this.restWebstartMockMvc = MockMvcBuilders.standaloneSetup(webstartResource).build();
    }

    @Before
    public void initTest() {
        webstart = new Webstart();
    }

    @Test
    @Transactional
    public void createWebstart() throws Exception {
        int databaseSizeBeforeCreate = webstartRepository.findAll().size();

        // Create the Webstart
        restWebstartMockMvc.perform(post("/api/webstarts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(webstart)))
                .andExpect(status().isCreated());

        // Validate the Webstart in the database
        List<Webstart> webstarts = webstartRepository.findAll();
        assertThat(webstarts).hasSize(databaseSizeBeforeCreate + 1);
        Webstart testWebstart = webstarts.get(webstarts.size() - 1);
    }

    @Test
    @Transactional
    public void getAllWebstarts() throws Exception {
        // Initialize the database
        webstartRepository.saveAndFlush(webstart);

        // Get all the webstarts
        restWebstartMockMvc.perform(get("/api/webstarts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(webstart.getId().intValue())));
    }

    @Test
    @Transactional
    public void getWebstart() throws Exception {
        // Initialize the database
        webstartRepository.saveAndFlush(webstart);

        // Get the webstart
        restWebstartMockMvc.perform(get("/api/webstarts/{id}", webstart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(webstart.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingWebstart() throws Exception {
        // Get the webstart
        restWebstartMockMvc.perform(get("/api/webstarts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWebstart() throws Exception {
        // Initialize the database
        webstartRepository.saveAndFlush(webstart);

		int databaseSizeBeforeUpdate = webstartRepository.findAll().size();

        // Update the webstart
        restWebstartMockMvc.perform(put("/api/webstarts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(webstart)))
                .andExpect(status().isOk());

        // Validate the Webstart in the database
        List<Webstart> webstarts = webstartRepository.findAll();
        assertThat(webstarts).hasSize(databaseSizeBeforeUpdate);
        Webstart testWebstart = webstarts.get(webstarts.size() - 1);
    }

    @Test
    @Transactional
    public void deleteWebstart() throws Exception {
        // Initialize the database
        webstartRepository.saveAndFlush(webstart);

		int databaseSizeBeforeDelete = webstartRepository.findAll().size();

        // Get the webstart
        restWebstartMockMvc.perform(delete("/api/webstarts/{id}", webstart.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Webstart> webstarts = webstartRepository.findAll();
        assertThat(webstarts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
