package com.bafconsulting.birdtheatre.web.rest;

import com.bafconsulting.birdtheatre.Application;
import com.bafconsulting.birdtheatre.domain.WebstartConfigurator;
import com.bafconsulting.birdtheatre.repository.WebstartConfiguratorRepository;
import com.bafconsulting.birdtheatre.repository.search.WebstartConfiguratorSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the WebstartConfiguratorResource REST controller.
 *
 * @see WebstartConfiguratorResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class WebstartConfiguratorResourceTest {

    private static final String DEFAULT_HOSTNAME = "SAMPLE_TEXT";
    private static final String UPDATED_HOSTNAME = "UPDATED_TEXT";
    private static final String DEFAULT_IPADDRESS = "SAMPLE_TEXT";
    private static final String UPDATED_IPADDRESS = "UPDATED_TEXT";

    @Inject
    private WebstartConfiguratorRepository webstartConfiguratorRepository;

    @Inject
    private WebstartConfiguratorSearchRepository webstartConfiguratorSearchRepository;

    private MockMvc restWebstartConfiguratorMockMvc;

    private WebstartConfigurator webstartConfigurator;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        WebstartConfiguratorResource webstartConfiguratorResource = new WebstartConfiguratorResource();
        ReflectionTestUtils.setField(webstartConfiguratorResource, "webstartConfiguratorRepository", webstartConfiguratorRepository);
        ReflectionTestUtils.setField(webstartConfiguratorResource, "webstartConfiguratorSearchRepository", webstartConfiguratorSearchRepository);
        this.restWebstartConfiguratorMockMvc = MockMvcBuilders.standaloneSetup(webstartConfiguratorResource).build();
    }

    @Before
    public void initTest() {
        webstartConfigurator = new WebstartConfigurator();
        webstartConfigurator.setHostname(DEFAULT_HOSTNAME);
        webstartConfigurator.setIpaddress(DEFAULT_IPADDRESS);
    }

    @Test
    @Transactional
    public void createWebstartConfigurator() throws Exception {
        int databaseSizeBeforeCreate = webstartConfiguratorRepository.findAll().size();

        // Create the WebstartConfigurator
        restWebstartConfiguratorMockMvc.perform(post("/api/webstartConfigurators")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(webstartConfigurator)))
                .andExpect(status().isCreated());

        // Validate the WebstartConfigurator in the database
        List<WebstartConfigurator> webstartConfigurators = webstartConfiguratorRepository.findAll();
        assertThat(webstartConfigurators).hasSize(databaseSizeBeforeCreate + 1);
        WebstartConfigurator testWebstartConfigurator = webstartConfigurators.get(webstartConfigurators.size() - 1);
        assertThat(testWebstartConfigurator.getHostname()).isEqualTo(DEFAULT_HOSTNAME);
        assertThat(testWebstartConfigurator.getIpaddress()).isEqualTo(DEFAULT_IPADDRESS);
    }

    @Test
    @Transactional
    public void getAllWebstartConfigurators() throws Exception {
        // Initialize the database
        webstartConfiguratorRepository.saveAndFlush(webstartConfigurator);

        // Get all the webstartConfigurators
        restWebstartConfiguratorMockMvc.perform(get("/api/webstartConfigurators"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(webstartConfigurator.getId().intValue())))
                .andExpect(jsonPath("$.[*].hostname").value(hasItem(DEFAULT_HOSTNAME.toString())))
                .andExpect(jsonPath("$.[*].ipaddress").value(hasItem(DEFAULT_IPADDRESS.toString())));
    }

    @Test
    @Transactional
    public void getWebstartConfigurator() throws Exception {
        // Initialize the database
        webstartConfiguratorRepository.saveAndFlush(webstartConfigurator);

        // Get the webstartConfigurator
        restWebstartConfiguratorMockMvc.perform(get("/api/webstartConfigurators/{id}", webstartConfigurator.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(webstartConfigurator.getId().intValue()))
            .andExpect(jsonPath("$.hostname").value(DEFAULT_HOSTNAME.toString()))
            .andExpect(jsonPath("$.ipaddress").value(DEFAULT_IPADDRESS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingWebstartConfigurator() throws Exception {
        // Get the webstartConfigurator
        restWebstartConfiguratorMockMvc.perform(get("/api/webstartConfigurators/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWebstartConfigurator() throws Exception {
        // Initialize the database
        webstartConfiguratorRepository.saveAndFlush(webstartConfigurator);

		int databaseSizeBeforeUpdate = webstartConfiguratorRepository.findAll().size();

        // Update the webstartConfigurator
        webstartConfigurator.setHostname(UPDATED_HOSTNAME);
        webstartConfigurator.setIpaddress(UPDATED_IPADDRESS);
        restWebstartConfiguratorMockMvc.perform(put("/api/webstartConfigurators")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(webstartConfigurator)))
                .andExpect(status().isOk());

        // Validate the WebstartConfigurator in the database
        List<WebstartConfigurator> webstartConfigurators = webstartConfiguratorRepository.findAll();
        assertThat(webstartConfigurators).hasSize(databaseSizeBeforeUpdate);
        WebstartConfigurator testWebstartConfigurator = webstartConfigurators.get(webstartConfigurators.size() - 1);
        assertThat(testWebstartConfigurator.getHostname()).isEqualTo(UPDATED_HOSTNAME);
        assertThat(testWebstartConfigurator.getIpaddress()).isEqualTo(UPDATED_IPADDRESS);
    }

    @Test
    @Transactional
    public void deleteWebstartConfigurator() throws Exception {
        // Initialize the database
        webstartConfiguratorRepository.saveAndFlush(webstartConfigurator);

		int databaseSizeBeforeDelete = webstartConfiguratorRepository.findAll().size();

        // Get the webstartConfigurator
        restWebstartConfiguratorMockMvc.perform(delete("/api/webstartConfigurators/{id}", webstartConfigurator.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<WebstartConfigurator> webstartConfigurators = webstartConfiguratorRepository.findAll();
        assertThat(webstartConfigurators).hasSize(databaseSizeBeforeDelete - 1);
    }
}
