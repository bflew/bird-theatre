package com.bafconsulting.birdtheatre.repository.search;

import com.bafconsulting.birdtheatre.domain.WebstartConfigurator;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the WebstartConfigurator entity.
 */
public interface WebstartConfiguratorSearchRepository extends ElasticsearchRepository<WebstartConfigurator, Long> {
}
