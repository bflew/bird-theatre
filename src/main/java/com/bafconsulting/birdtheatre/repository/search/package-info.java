/**
 * Spring Data ElasticSearch repositories.
 */
package com.bafconsulting.birdtheatre.repository.search;
