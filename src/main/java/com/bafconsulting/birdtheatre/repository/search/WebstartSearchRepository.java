package com.bafconsulting.birdtheatre.repository.search;

import com.bafconsulting.birdtheatre.domain.Webstart;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Webstart entity.
 */
public interface WebstartSearchRepository extends ElasticsearchRepository<Webstart, Long> {
}
