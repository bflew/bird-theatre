package com.bafconsulting.birdtheatre.repository;

import com.bafconsulting.birdtheatre.domain.Webstart;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Webstart entity.
 */
public interface WebstartRepository extends JpaRepository<Webstart,Long> {

}
