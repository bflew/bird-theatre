package com.bafconsulting.birdtheatre.repository;

import com.bafconsulting.birdtheatre.domain.WebstartConfigurator;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the WebstartConfigurator entity.
 */
public interface WebstartConfiguratorRepository extends JpaRepository<WebstartConfigurator,Long> {

}
