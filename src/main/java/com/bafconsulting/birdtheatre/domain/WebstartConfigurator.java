package com.bafconsulting.birdtheatre.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A WebstartConfigurator.
 */
@Entity
@Table(name = "WEBSTARTCONFIGURATOR")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="webstartconfigurator")
public class WebstartConfigurator implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "hostname")
    private String hostname;

    @Column(name = "ipaddress")
    private String ipaddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WebstartConfigurator webstartConfigurator = (WebstartConfigurator) o;

        if ( ! Objects.equals(id, webstartConfigurator.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "WebstartConfigurator{" +
                "id=" + id +
                ", hostname='" + hostname + "'" +
                ", ipaddress='" + ipaddress + "'" +
                '}';
    }
}
