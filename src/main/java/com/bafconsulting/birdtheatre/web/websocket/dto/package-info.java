/**
 * Data Access Objects used by WebSocket services.
 */
package com.bafconsulting.birdtheatre.web.websocket.dto;
