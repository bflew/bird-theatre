package com.bafconsulting.birdtheatre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bafconsulting.birdtheatre.domain.Webstart;
import com.bafconsulting.birdtheatre.repository.WebstartRepository;
import com.bafconsulting.birdtheatre.repository.search.WebstartSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Webstart.
 */
@RestController
@RequestMapping("/api")
public class WebstartResource {

    private final Logger log = LoggerFactory.getLogger(WebstartResource.class);

    @Inject
    private WebstartRepository webstartRepository;

    @Inject
    private WebstartSearchRepository webstartSearchRepository;

    /**
     * POST  /webstarts -> Create a new webstart.
     */
    @RequestMapping(value = "/webstarts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Webstart> create(@RequestBody Webstart webstart) throws URISyntaxException {
        log.debug("REST request to save Webstart : {}", webstart);
        if (webstart.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new webstart cannot already have an ID").body(null);
        }
        Webstart result = webstartRepository.save(webstart);
        webstartSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/webstarts/" + webstart.getId())).body(result);
    }

    /**
     * PUT  /webstarts -> Updates an existing webstart.
     */
    @RequestMapping(value = "/webstarts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Webstart> update(@RequestBody Webstart webstart) throws URISyntaxException {
        log.debug("REST request to update Webstart : {}", webstart);
        if (webstart.getId() == null) {
            return create(webstart);
        }
        Webstart result = webstartRepository.save(webstart);
        webstartSearchRepository.save(webstart);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /webstarts -> get all the webstarts.
     */
    @RequestMapping(value = "/webstarts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Webstart> getAll() {
        log.debug("REST request to get all Webstarts");
        return webstartRepository.findAll();
    }

    /**
     * GET  /webstarts/:id -> get the "id" webstart.
     */
    @RequestMapping(value = "/webstarts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Webstart> get(@PathVariable Long id) {
        log.debug("REST request to get Webstart : {}", id);
        return Optional.ofNullable(webstartRepository.findOne(id))
            .map(webstart -> new ResponseEntity<>(
                webstart,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /webstarts/:id -> delete the "id" webstart.
     */
    @RequestMapping(value = "/webstarts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Webstart : {}", id);
        webstartRepository.delete(id);
        webstartSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/webstarts/:query -> search for the webstart corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/webstarts/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Webstart> search(@PathVariable String query) {
        return StreamSupport
            .stream(webstartSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
