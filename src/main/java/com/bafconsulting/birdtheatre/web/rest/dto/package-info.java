/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.bafconsulting.birdtheatre.web.rest.dto;
