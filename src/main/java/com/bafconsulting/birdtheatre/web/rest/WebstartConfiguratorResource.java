package com.bafconsulting.birdtheatre.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bafconsulting.birdtheatre.domain.WebstartConfigurator;
import com.bafconsulting.birdtheatre.repository.WebstartConfiguratorRepository;
import com.bafconsulting.birdtheatre.repository.search.WebstartConfiguratorSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing WebstartConfigurator.
 */
@RestController
@RequestMapping("/api")
public class WebstartConfiguratorResource {

    private final Logger log = LoggerFactory.getLogger(WebstartConfiguratorResource.class);

    @Inject
    private WebstartConfiguratorRepository webstartConfiguratorRepository;

    @Inject
    private WebstartConfiguratorSearchRepository webstartConfiguratorSearchRepository;

    /**
     * POST  /webstartConfigurators -> Create a new webstartConfigurator.
     */
    @RequestMapping(value = "/webstartConfigurators",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WebstartConfigurator> create(@RequestBody WebstartConfigurator webstartConfigurator) throws URISyntaxException {
        log.debug("REST request to save WebstartConfigurator : {}", webstartConfigurator);
        if (webstartConfigurator.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new webstartConfigurator cannot already have an ID").body(null);
        }
        WebstartConfigurator result = webstartConfiguratorRepository.save(webstartConfigurator);
        webstartConfiguratorSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/webstartConfigurators/" + webstartConfigurator.getId())).body(result);
    }

    /**
     * PUT  /webstartConfigurators -> Updates an existing webstartConfigurator.
     */
    @RequestMapping(value = "/webstartConfigurators",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WebstartConfigurator> update(@RequestBody WebstartConfigurator webstartConfigurator) throws URISyntaxException {
        log.debug("REST request to update WebstartConfigurator : {}", webstartConfigurator);
        if (webstartConfigurator.getId() == null) {
            return create(webstartConfigurator);
        }
        WebstartConfigurator result = webstartConfiguratorRepository.save(webstartConfigurator);
        webstartConfiguratorSearchRepository.save(webstartConfigurator);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET  /webstartConfigurators -> get all the webstartConfigurators.
     */
    @RequestMapping(value = "/webstartConfigurators",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<WebstartConfigurator> getAll() {
        log.debug("REST request to get all WebstartConfigurators");
        return webstartConfiguratorRepository.findAll();
    }

    /**
     * GET  /webstartConfigurators/:id -> get the "id" webstartConfigurator.
     */
    @RequestMapping(value = "/webstartConfigurators/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<WebstartConfigurator> get(@PathVariable Long id) {
        log.debug("REST request to get WebstartConfigurator : {}", id);
        return Optional.ofNullable(webstartConfiguratorRepository.findOne(id))
            .map(webstartConfigurator -> new ResponseEntity<>(
                webstartConfigurator,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /webstartConfigurators/:id -> delete the "id" webstartConfigurator.
     */
    @RequestMapping(value = "/webstartConfigurators/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete WebstartConfigurator : {}", id);
        webstartConfiguratorRepository.delete(id);
        webstartConfiguratorSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/webstartConfigurators/:query -> search for the webstartConfigurator corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/webstartConfigurators/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<WebstartConfigurator> search(@PathVariable String query) {
        return StreamSupport
            .stream(webstartConfiguratorSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
