package com.bafconsulting.birdtheatre.service.impl;

import com.bafconsulting.birdtheatre.service.WebstartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WebstartServiceImpl implements WebstartService {

    private final Logger log = LoggerFactory.getLogger(WebstartServiceImpl.class);

}
