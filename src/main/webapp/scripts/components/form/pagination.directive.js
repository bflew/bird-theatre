/* globals $ */
'use strict';

angular.module('birdtheatreApp')
    .directive('birdtheatreAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
