/* globals $ */
'use strict';

angular.module('birdtheatreApp')
    .directive('birdtheatreAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
