'use strict';

angular.module('birdtheatreApp')
    .factory('WebstartConfigurator', function ($resource, DateUtils) {
        return $resource('api/webstartConfigurators/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
