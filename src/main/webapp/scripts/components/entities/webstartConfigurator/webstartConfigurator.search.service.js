'use strict';

angular.module('birdtheatreApp')
    .factory('WebstartConfiguratorSearch', function ($resource) {
        return $resource('api/_search/webstartConfigurators/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
