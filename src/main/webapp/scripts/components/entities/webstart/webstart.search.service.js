'use strict';

angular.module('birdtheatreApp')
    .factory('WebstartSearch', function ($resource) {
        return $resource('api/_search/webstarts/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
