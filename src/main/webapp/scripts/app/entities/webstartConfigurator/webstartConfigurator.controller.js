'use strict';

angular.module('birdtheatreApp')
    .controller('WebstartConfiguratorController', function ($scope, WebstartConfigurator, WebstartConfiguratorSearch) {
        $scope.webstartConfigurators = [];
        $scope.loadAll = function() {
            WebstartConfigurator.query(function(result) {
               $scope.webstartConfigurators = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            WebstartConfigurator.get({id: id}, function(result) {
                $scope.webstartConfigurator = result;
                $('#deleteWebstartConfiguratorConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            WebstartConfigurator.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteWebstartConfiguratorConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            WebstartConfiguratorSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.webstartConfigurators = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.webstartConfigurator = {hostname: null, ipaddress: null, id: null};
        };
    });
