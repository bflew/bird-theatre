'use strict';

angular.module('birdtheatreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('webstartConfigurator', {
                parent: 'entity',
                url: '/webstartConfigurators',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'birdtheatreApp.webstartConfigurator.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/webstartConfigurator/webstartConfigurators.html',
                        controller: 'WebstartConfiguratorController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('webstartConfigurator');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('webstartConfigurator.detail', {
                parent: 'entity',
                url: '/webstartConfigurator/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'birdtheatreApp.webstartConfigurator.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/webstartConfigurator/webstartConfigurator-detail.html',
                        controller: 'WebstartConfiguratorDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('webstartConfigurator');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'WebstartConfigurator', function($stateParams, WebstartConfigurator) {
                        return WebstartConfigurator.get({id : $stateParams.id});
                    }]
                }
            })
            .state('webstartConfigurator.new', {
                parent: 'webstartConfigurator',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/webstartConfigurator/webstartConfigurator-dialog.html',
                        controller: 'WebstartConfiguratorDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {hostname: null, ipaddress: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('webstartConfigurator', null, { reload: true });
                    }, function() {
                        $state.go('webstartConfigurator');
                    })
                }]
            })
            .state('webstartConfigurator.edit', {
                parent: 'webstartConfigurator',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/webstartConfigurator/webstartConfigurator-dialog.html',
                        controller: 'WebstartConfiguratorDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['WebstartConfigurator', function(WebstartConfigurator) {
                                return WebstartConfigurator.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('webstartConfigurator', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
