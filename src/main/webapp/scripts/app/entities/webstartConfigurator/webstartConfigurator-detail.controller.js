'use strict';

angular.module('birdtheatreApp')
    .controller('WebstartConfiguratorDetailController', function ($scope, $rootScope, $stateParams, entity, WebstartConfigurator) {
        $scope.webstartConfigurator = entity;
        $scope.load = function (id) {
            WebstartConfigurator.get({id: id}, function(result) {
                $scope.webstartConfigurator = result;
            });
        };
        $rootScope.$on('birdtheatreApp:webstartConfiguratorUpdate', function(event, result) {
            $scope.webstartConfigurator = result;
        });
    });
