'use strict';

angular.module('birdtheatreApp').controller('WebstartConfiguratorDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'WebstartConfigurator',
        function($scope, $stateParams, $modalInstance, entity, WebstartConfigurator) {

        $scope.webstartConfigurator = entity;
        $scope.load = function(id) {
            WebstartConfigurator.get({id : id}, function(result) {
                $scope.webstartConfigurator = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('birdtheatreApp:webstartConfiguratorUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.webstartConfigurator.id != null) {
                WebstartConfigurator.update($scope.webstartConfigurator, onSaveFinished);
            } else {
                WebstartConfigurator.save($scope.webstartConfigurator, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
