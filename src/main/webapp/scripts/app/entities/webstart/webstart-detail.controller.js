'use strict';

angular.module('birdtheatreApp')
    .controller('WebstartDetailController', function ($scope, $rootScope, $stateParams, entity, Webstart) {
        $scope.webstart = entity;
        $scope.load = function (id) {
            Webstart.get({id: id}, function(result) {
                $scope.webstart = result;
            });
        };
        $rootScope.$on('birdtheatreApp:webstartUpdate', function(event, result) {
            $scope.webstart = result;
        });
    });
