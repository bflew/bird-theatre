'use strict';

angular.module('birdtheatreApp')
    .controller('WebstartController', function ($scope, Webstart, WebstartSearch) {
        $scope.webstarts = [];
        $scope.loadAll = function() {
            Webstart.query(function(result) {
               $scope.webstarts = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Webstart.get({id: id}, function(result) {
                $scope.webstart = result;
                $('#deleteWebstartConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Webstart.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteWebstartConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            WebstartSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.webstarts = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.webstart = {id: null};
        };
    });
