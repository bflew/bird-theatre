'use strict';

angular.module('birdtheatreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('webstart', {
                parent: 'entity',
                url: '/webstarts',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'birdtheatreApp.webstart.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/webstart/webstarts.html',
                        controller: 'WebstartController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('webstart');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('webstart.detail', {
                parent: 'entity',
                url: '/webstart/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'birdtheatreApp.webstart.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/webstart/webstart-detail.html',
                        controller: 'WebstartDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('webstart');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Webstart', function($stateParams, Webstart) {
                        return Webstart.get({id : $stateParams.id});
                    }]
                }
            })
            .state('webstart.new', {
                parent: 'webstart',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/webstart/webstart-dialog.html',
                        controller: 'WebstartDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('webstart', null, { reload: true });
                    }, function() {
                        $state.go('webstart');
                    })
                }]
            })
            .state('webstart.edit', {
                parent: 'webstart',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/webstart/webstart-dialog.html',
                        controller: 'WebstartDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Webstart', function(Webstart) {
                                return Webstart.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('webstart', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
