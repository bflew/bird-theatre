'use strict';

angular.module('birdtheatreApp').controller('WebstartDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Webstart',
        function($scope, $stateParams, $modalInstance, entity, Webstart) {

        $scope.webstart = entity;
        $scope.load = function(id) {
            Webstart.get({id : id}, function(result) {
                $scope.webstart = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('birdtheatreApp:webstartUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.webstart.id != null) {
                Webstart.update($scope.webstart, onSaveFinished);
            } else {
                Webstart.save($scope.webstart, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
