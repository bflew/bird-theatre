'use strict';

angular.module('birdtheatreApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
