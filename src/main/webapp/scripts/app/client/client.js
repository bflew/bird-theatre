'use strict';

angular.module('birdtheatreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('client', {
                abstract: true,
                parent: 'site'
            });
    });
