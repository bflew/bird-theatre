'use strict';

angular.module('birdtheatreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('webstart', {
                parent: 'client',
                url: '/webstart',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'webstart.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/client/webstart/webstart.html',
                        controller: 'WebStartController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('webstart');
                        return $translate.refresh();
                    }]
                }
            });
    });
